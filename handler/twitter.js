'use strict';
const axios = require('axios');
const Twitter = require('twitter');

const headers = {
  "Access-Control-Allow-Credentials": true,
  "Access-Control-Allow-Headers": "Content-Type",
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Methods": "*",
  "Content-Type": "application/json",
};

const _tweetHelper = async (event) => {
  let client = new Twitter({
    consumer_key: process.env.consumer_key,
    consumer_secret: process.env.consumer_secret,
    access_token_key: process.env.access_token_key,
    access_token_secret: process.env.access_token_secret
  });
  console.log(client);
  let params = {
    screen_name: event.queryStringParameters.topic || "",
    count: 10
  };


  const promises = [];
  promises.push(new Promise((resolve, reject) => {
    console.log('downloading time');

    client.get('statuses/user_timeline', params, function(error, tweets, response) {
      console.log(error);
      if (!error) {
        resolve(tweets);
      } else {
        resolve(tweets);
      }
    });
}))
return await Promise.all(promises);
}

module.exports.handle = async (event) => {
  let tweets = await _tweetHelper(event);
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Tweets fetched successfully',
        data: tweets[0],
      }
    ),
    headers: headers
  };
};
